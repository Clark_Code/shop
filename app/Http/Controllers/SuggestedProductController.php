<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;
use App\Library\Services\SuggestProduct;

class SuggestedProductController extends Controller
{

    public function __construct(SuggestProduct $suggest){
        $this->suggest = $suggest;
    }
    /**
     * Get all possible suggestions for the given product
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Displays a limited selection of similar products
     * @param int $limit
     * @param int $id
     * @return \Illuminate\http\Response
     */

    public function getLimitedSelection($attributeName, $attributeValue){

        // get suggested items if they have the following relationship
        $suggestedTest = Product::whereHas(
            'attributes', 
            function($query) use ($attributeName, $attributeValue){
                // Every model is checked to see if their name and values are correct
                $query->where('name', $attributeName)->where('value',$attributeValue);
            }
            
        // Limit the amount by four and run the query
        )->limit(4)->get();
        
        $collection = new ProductCollection($suggestedTest);
        return $collection->all();
    }   

    public function get_suggested(Product $product){
        return $this->suggest->suggestProduct($product);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
