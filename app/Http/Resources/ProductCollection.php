<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Product as ProductResource;

class ProductCollection extends ResourceCollection{

    public function toArray($request){
        return ProductResource::collection($this->collection);
    }
}

?>