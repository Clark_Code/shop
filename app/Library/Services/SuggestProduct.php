<?php 

namespace App\Library\Services;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Product as ProductResource;
use App\Product;
use Illuminate\Support\Collection;




class SuggestProduct{

    public function suggestProduct(Product $product){
        $product_resource = new ProductResource($product);
        $product_weights = [];

        $result = Product::with('attributes')
        ->where('type', $product->type)
        ->where('id', '!=', $product->id)
        ->get()
        ->each(function($item, $key) use($product_resource, &$product_weights) {
            $match_count = 0;

            if($item->attributes){
                $product_attributes =  $this->read_product_attributes($product_resource);
                $collection = $this->read_product_attributes($item);

                foreach($collection as $suggested_key => $suggested_value){

                    $attribute_match = $collection->contains(function ($value, $key) use ($suggested_key){
                            
                        if($key == $suggested_key) return true;
                    });

                    if($attribute_match){
                        $match_count++;
                        if(!isset($product_weights[$item->id]))
                            $product_weights[$item->id] = new SuggestedProduct($match_count, $item);
                            
                        else
                            $product_weights[$item->id]->weight = $match_count;                    
                    }
                }
            }
        });

            asort($product_weights);
            return $product_weights;
    }

    private function read_product_attributes($product){
        $result = [];

        foreach($product->attributes as $item){
            
            $result += [$item->pivot->attribute_id => $item->pivot->value];
        }
        return collect($result);
        
    }
}

class SuggestedProduct{
    public function __construct(int $weight, Product $product){

        $this->weight = $weight;
        $this->product = new ProductResource($product); // Resources are easier when working client side
        $this->product_id = $product->id;
    }
    
}


?>