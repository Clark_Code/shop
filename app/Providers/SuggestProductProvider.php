<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\SuggestProduct;

class SuggestProductProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Library\Services\SuggestProduct', function($app){
            return new SuggestProduct();
        });
    }
}
