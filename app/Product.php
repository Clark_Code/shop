<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function attributes(){

        return $this->belongsToMany(
            'App\Attributes', // Model we wish to relate
            'product_attributes', // The pivot table
            'product_id',
            'attribute_id',
        )->withPivot('value');
    }

    public function getAtIdAttribute(){
        return $this->pivot;
    }

    public function getAtNameAttribute(){
        return $this;
    }

    public function getAtValueAttribute(){
        return $this->pivot->value;
    }
    
}
