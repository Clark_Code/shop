/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import App from './App.vue';
import Vue from 'vue';
import Store from './store/Store';

// import components for the vue router
import home from './components/home/home.vue'
import shop from './components/shop/Shop.vue'
import VueRouter from 'vue-router';
import contact from './components/contact/contact.vue'
import ProductView from './components/shop/product-view/ProductView'

const routes = [
  // Shop routes
  // Used to group any shop based routes and  the initial shop homepage
  {
    path: '/', component: shop
  },

  {
    path: '/shop/product/:id',
    name: 'productView',
    component: ProductView,
    props: true
  },
// Misc routes
// Used to store any routes that aren't related to the shop
  {
    path: '/contact', component: contact
  },
];

const router = new VueRouter({
  mode: 'history',
  routes: routes
});

window.Vue = require('vue');
Vue.use(VueRouter);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


new Vue({
  store: Store,
  router,
  el: '#app',
  components: {App},
  render: h => h(App)
});
