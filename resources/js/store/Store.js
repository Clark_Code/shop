import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex);

Axios.defaults.baseURL = '/api/';

// Create the store for our Vuex
const store = new Vuex.Store({
    state: {
        shop:{
            // Products is used to store all the products from the database
            // ShopFloor.vue will pull in these and render them on screen
            products: {
                
            },

            suggestedProducts: {
                
            }
        },

        ui: {
            // The ID of the last product the user visted
            featching_product: false
        }
    },

    // Used to edit the values in the Store.state
    mutations:{
        addProducts(state, products){

            state.shop.products = {

                ...state.shop.products,
                // Add on the new products but reduce them into a single object
                ...products.reduce((accum,prod) => {

                    // The product ID will be the attribute name inside products
                    accum[prod.id] = prod;
                    return accum;
                }, {})
            }
        },

        addProduct(state, product){
            // state.shop.products[product.id] = product;
            Vue.set(state.shop.products,product.id,product);
        },

        add_suggested_products(state, suggested){
            
            state.shop.suggestedProducts = {
                ...suggested
            }

            console.log(state.shop.suggestedProducts);
            console.log(state.shop.products);
        },

        set_last_viewed_product(state, product){
            state.ui.product_view = product;
        }
    },

    // Components call actions to use the mutations
    actions: {
        
        getProducts(context){
            Axios.get("/product").then((res) => {
                context.commit('addProducts', res.data);
            });
        },

        fetch_suggested_products(context,product_id){
            Axios.get(`/product/suggest/${product_id}`).then((res) => {
                context.commit('add_suggested_products',res.data);
            })
        },

        fetch_product_by_id(context,product_id){
           return Axios.get(`/product/${product_id}`, product_id).then( (res) => {
                context.commit('addProduct', res.data);
            });
        }
    },

    getters: {
        products(state){
            return state.shop.products;
        },

        product_by_id: (state, getters) => (id) =>{
            let product =  getters.products[id];
        
            if (!product){ 
                store.dispatch('fetch_product_by_id',id);
                return null;
            }
            else return product;
        },


        suggested: (state) =>{
            return state.shop.suggestedProducts;
        }
    }
});

export default store;